FROM ubuntu:latest

RUN apt update && \
    DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt install -y tzdata \
    && \
    apt install -y --no-install-recommends \
        libssl-dev \
        curl \
        git \
        gnupg \
        jq \
        software-properties-common \
    && \
    add-apt-repository ppa:git-core/ppa && \
    apt update -qq && \
    apt install -y --no-install-recommends \
        dirmngr \
        software-properties-common \
    && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
    add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" \
    && \
    apt install -y --no-install-recommends \
        build-essential \
        libcurl4-openssl-dev \
        libfontconfig1-dev \
        libfreetype6-dev \
        libfribidi-dev \
        libharfbuzz-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff5-dev \
        libxml2-dev \
        libxslt1-dev \
    && \
    apt install -y --no-install-recommends \
        r-base \
    && \
    apt install -y \
        $(curl https://carpentries.r-universe.dev/stats/sysdeps 2> /dev/null | jq -r '.headers[0] | select(. != null)') 2> /dev/null \
    && \
    curl -L https://github.com/jgm/pandoc/releases/download/2.18/pandoc-2.18-1-amd64.deb -o /tmp/pandoc.deb && \
    apt install -y --no-install-recommends /tmp/pandoc.deb && \
    rm -f /tmp/pandoc.deb

COPY /install_deps.R /tmp/install_deps.R
RUN Rscript /tmp/install_deps.R

RUN useradd --uid 1000 --gid users -m user
USER user

ENTRYPOINT /usr/bin/R
